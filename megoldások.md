# Megoldások

## 1. előadás

### Elennőrző kérdések (26-27. diák)

1. B
2. D
3. A
4. C
5. D

### Elennőrző kérdések (28. dia)

1. I
2. I
3. I
4. I

## 2. előadás

###  Költségszámítás (11. dia)

| **Tétel**                               | **Kiadás** | **Költség** | **Ráfordítás** |
| --------------------------------------- | :--------: | :---------: | :------------: |
| Bankszámláról rendezett hitelkamat      |     X      |             |       X        |
| Anyagbeszerzés készpénzért              |     X      |             |                |
| Berendezések értékcsökkenési leírása    |            |      X      |       X        |
| Cégautó adó rendezése bankszámláról     |     X      |             |                |
| Éves iparűzési adó elszámolása          |            |             |       X        |
| Szolgáltatáshoz felhasznált anyagok     |            |      X      |       X        |
| Bruttó bér elszámolása                  |            |      X      |       X        |
| Eladott áruk beszerzési értéke          |            |             |       X        |
| Irodaszer vásárlás készpénzért          |     X      |      X      |       X        |
| Nettó bér kifizetése                    |     X      |             |                |
| Egészségügyi hozzájárulás számfejtése   |            |      X      |       X        |
| Alapítványnak átutalt támogatás összege |     X      |             |       X        |

### Költségszámítás (17. dia)
| **Gazdasági esemény megnevezése**       | **Anyagjellegű kiadás** | **Személyi jellegű kiadás** | **Értékcsökkenési leírás** | **Egyik sem** |
| --------------------------------------- | :---------------------: | :-------------------------: | :------------------------: | :-----------: |
| Havi karbantartási költség összege      |            X            |                             |                            |               |
| Eszközök terven felüli értékcsökkenése  |                         |                             |                            |       X       |
| Dolgozóknak számfejtett bruttó bér      |                         |              X              |                            |               |
| Partner cégnek átutalt késedelmi kamat  |                         |                             |                            |       X       |
| Telefonköltség számla szerinti összege  |            X            |                             |                            |               |
| Immateriális javak értékcsökkenése      |                         |                             |             X              |               |
| Időszaki vízdíj számla szerinti összege |            X            |                             |                            |               |
| Eladott áruk beszerzési ára             |                         |                             |                            |       X       |
| Kapott kölcsön után fizetett kamat      |                         |                             |                            |       X       |
| Irodaszer, nyomtatványok vásárlása      |            X            |                             |                            |               |

### Költségszámítás (19. dia)

| **Felmerült költség**                              | **Közvetlen** | **Közvetett** |
| -------------------------------------------------- | :-----------: | :-----------: |
| Asztalos műhely épületének értékcsökkenési leírása |               |       X       |
| Könyvelési díj                                     |               |       X       |
| Székgyártáshoz szükséges faanyag                   |       X       |               |
| Asztalos tanoncok havi munkabére                   |       X       |               |
| Ügyvezető igazgató autójának karbantartási díja    |               |       X       |
| Asztalgyártáshoz szükséges ragasztó                |       X       |               |
| Bútorgyártáshoz felhasznált üveg                   |       X       |               |
| Ügyvezető igazgató bruttó bére                     |               |       X       |
| Könyvvizsgálati díj                                |               |       X       |
| Tevékenységhez szükséges kisösszegű berendezések   |               |       X       |

### Költségelemzés (28. dia)

| **Megnevezés**         | **Volumenváltozás** | **Árváltozás** | **Proporcionális költség változása** | **Fix költség változása** |
| ---------------------- | :-----------------: | :------------: | :----------------------------------: | :-----------------------: |
| Árbevétel              |       43 200        |     41 200     |                40 000                |          40 000           |
| Proporcionális költség |       27 000        |     25 000     |                23 800                |          25 000           |
| Fedezeti összeg        |       16 200        |     16 200     |                16 200                |          15 000           |
| Fix költség            |        6 000        |     6 000      |                6 000                 |           4 800           |
| Eredmény               |       10 200        |     10 200     |                10 200                |          10 200           |
| Változás mértéke (%)   |          8          |       3        |                 -4.8                 |            -20            |

### Ellenőrző kérdések (44. dia)

1. C
2. B
3. D

### Ellenőrző kérdések (45. dia)

1. I
2. I
3. I
4. I

## 3. hét

Itt nem voltak kitakarva a megoldások.

## 4. hét

### Költség-volumen-eredmény elemzés (5. dia)

| **Eset** | **Bevétel (ezer Ft)** | **Változó költség (ezer Ft)** | **Fix költség (ezer Ft)** | **Összes költség (ezer Ft)** | **Működési eredmény (ezer Ft)** | **Fedezeti hányad(%)** |
| :------: | :-------------------: | :---------------------------: | :-----------------------: | :--------------------------: | :-----------------------------: | :--------------------: |
|    1.    |         5 600         |             2 000             |           2 200           |            4 200             |              1 400              |          64.3          |
|    2.    |         3 000         |             1 800             |           2 600           |            4 400             |             -1 400              |          40.0          |
|    3.    |         4 000         |             8 00              |           1 200           |            2 000             |              2 000              |          80.0          |
|    4.    |         5 000         |             2 500             |            800            |            3 300             |              1 700              |          50.0          |

### Költség-volumen-eredmény elemzés (8. dia)

| **Megnevezés**            | **Eredeti állapot** | **?** | **?** |
| ------------------------- | :-----------------: | :---: | :---: |
| Árbevétel (ezer Ft)       |        1 600        | 3 000 | 3 150 |
| Változó költség (ezer Ft) |         960         | 1 800 | 1 890 |
| Fedezeti összeg (ezer Ft) |         640         | 1 200 | 1 260 |
| Fix költség (ezer Ft)     |         800         | 1 200 | 1 200 |
| Eredmény (ezer Ft)        |        -160         |   0   |  60   |
